//package tn.com.st2i.project.config;
//
//import java.util.Properties;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;
//
//@Configuration
//public class EmailConfig {
//	
//	@Value("${mail.host}")
//	private String host;
//	
//	@Value("${mail.port}")
//	private String port;
//	
//	@Value("${mail.username}")
//	private String username;
//	
//	@Value("${mail.password}")
//	private String password;
//	
//	@Value("${mail.transport.protocol}")
//	private String protocol;
//	
//	@Value("${mail.smtp.auth}")
//	private String auth;
//	
//	@Value("${mail.smtp.starttls.enable}")
//	private String enable;
//	
//	@Value("${mail.debug}")
//	private String debug;
//	
//	@Value("${mail.smtp.socketFactory.port}")
//	private String socketFactoryPort;
//	
//	@Value("${mail.smtp.socketFactory.class}")
//	private String socketFactoryClass;
//	
//	@Value("${mail.smtp.socketFactory.fallback}")
//	private String socketFactoryFallback;
//	
//	@Value("${mail.smtp.ssl.enable}")
//	private String sslEnable;
//
//
//
//	@Bean
//	public JavaMailSender getJavaMailSender() {
//		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//
//		mailSender.setHost(host);
//		mailSender.setPort(Integer.parseInt(port));
//
//		mailSender.setUsername(username);
//		mailSender.setPassword(password);
//
//		Properties props = mailSender.getJavaMailProperties();
//		props.put("mail.transport.protocol", protocol);
//		props.put("mail.smtp.auth", auth);
//		props.put("mail.smtp.starttls.enable", enable);
//		props.put("mail.debug", debug);
//		props.put("mail.smtp.socketFactory.port", socketFactoryPort);
//		props.put("mail.smtp.socketFactory.class", socketFactoryClass);
//		props.put("mail.smtp.socketFactory.fallback", socketFactoryFallback);
//		props.put("mail.smtp.ssl.enable", sslEnable);
//
//		return mailSender;
//	}
//
//}
