package tn.com.st2i.project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import net.sf.jasperreports.engine.SimpleJasperReportsContext;

@EnableEurekaClient
@SpringBootApplication
public class ReportingApplication {


	public static void main(String[] args) {
		SpringApplication.run(ReportingApplication.class, args);

	}

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public SimpleJasperReportsContext jasperReportsContext() {
	    return new SimpleJasperReportsContext();
	}
}
