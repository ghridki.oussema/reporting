package tn.com.st2i.project.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import tn.com.st2i.project.tools.model.SendObject;
import tn.com.st2i.project.view.model.VAdmUser;

public interface ISendWsService {

	public ResponseEntity<?> sendResult(HttpServletRequest request, SendObject so);

	public ResponseEntity<?> sendResultException(HttpServletRequest request);

	public ResponseEntity<?> sendResultPublic(HttpServletRequest request, SendObject so);
	
	public Long getIdCurrentUser(HttpServletRequest request);

    public VAdmUser getCurrentUser(HttpServletRequest request);

	ResponseEntity<?> downloadFile(HttpServletRequest request, SendObject so);

}
