package tn.com.st2i.project.common.service.impl;

/**
 * Enumerates the list of possible encoding types.
 *
 * @author Khadija FERJANI
 */
public enum TypeExport
{
	/**
	 * PDF report.
	 */
	PDF,
	/**
	 * EXCEL report.
	 */
	EXCEL,
	/**
	 * CSV report.
	 */
	CSV,
	/**
	 * RTF report.
	 */
	RTF;

	/**
	 * Indicates if the received export type is supported.
	 * 
	 * @param type the received type
	 * @return {@code true} if the support type is supported, {@code false} otherwise.
	 */
	public static boolean contains(Object type)
	{
		if (type == null) {
			// If tye type is null, use PDF !
			return true;
		}
		if (!(type instanceof String)) {
			return false;
		}
		try {
			TypeExport.valueOf((String) type);
			return true;
		} catch (IllegalArgumentException ex) {
			return false;
		}
	}
}
