package tn.com.st2i.project.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.st2i.project.service.ICommonService;
import tn.com.st2i.project.model.ParamEtatEdition;
import tn.com.st2i.project.repsitory.IParamEtatEditionRepository;
import tn.com.st2i.project.service.IParamEtatEditionService;
import tn.com.st2i.project.tools.ConstanteService;
import tn.com.st2i.project.tools.ConstanteWs;
import tn.com.st2i.project.tools.UtilsWs;
import tn.com.st2i.project.tools.model.SendObject;

@Service
public class ParamEtatEditionService implements IParamEtatEditionService {

	private static final Logger logger = LogManager.getLogger(ParamEtatEditionService.class);

	@Autowired
	private IParamEtatEditionRepository paramEtatEditionRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<ParamEtatEdition> getList() {
		try {
			return paramEtatEditionRepository.findAll();
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public ParamEtatEdition findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new ParamEtatEdition(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (ParamEtatEdition) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public ParamEtatEdition saveOrUpdate(ParamEtatEdition entity) {
		try {
			return paramEtatEditionRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			paramEtatEditionRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject getListParamByIdEtatWs(Long idetat) {
		try {
			if (idetat == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS,
					paramEtatEditionRepository.getListParamByIdEtat(idetat));
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method getListParamByIdEtatWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject findParamEtatEditionByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			ParamEtatEdition entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method findParamEtatEditionByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListParamEtatEditionWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method getListParamEtatEditionWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateParamEtatEditionWs(ParamEtatEdition entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method saveOrUpdateParamEtatEditionWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteParamEtatEditionByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error ParamEtatEditionService in method getListParamEtatEdition " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
