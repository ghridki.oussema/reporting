package tn.com.st2i.project.service;

import java.util.List;

import tn.com.st2i.project.model.ParamEtatEdition;
import tn.com.st2i.project.tools.model.SendObject;

public interface IParamEtatEditionService {

	public List<ParamEtatEdition> getList();

	public ParamEtatEdition findById(Long id);

	public ParamEtatEdition saveOrUpdate(ParamEtatEdition entity);

	public Boolean deleteById(Long id);

	public SendObject findParamEtatEditionByIdWs(Long id);

	public SendObject getListParamEtatEditionWs();

	public SendObject saveOrUpdateParamEtatEditionWs(ParamEtatEdition entity);

	public SendObject deleteParamEtatEditionByIdWs(Long id);
	
	public SendObject getListParamByIdEtatWs(Long idetat);

}
