package tn.com.st2i.project.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import tn.com.st2i.project.service.ICommonService;
import tn.com.st2i.project.model.EtatEdition;
import tn.com.st2i.project.model.ParamEtatEdition;
import tn.com.st2i.project.repsitory.IEtatEditionRepository;
import tn.com.st2i.project.service.IEtatEditionService;
import tn.com.st2i.project.tools.ConstanteService;
import tn.com.st2i.project.tools.ConstanteWs;
import tn.com.st2i.project.tools.UtilsWs;
import tn.com.st2i.project.tools.model.SendObject;

@Service
public class EtatEditionService implements IEtatEditionService {

	private static final Logger logger = LogManager.getLogger(EtatEditionService.class);

	@Autowired
	private IEtatEditionRepository etatEditionRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	private final DataSource dataSource;

	String path = "/etat/";

	public EtatEditionService(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<EtatEdition> getList() {
		try {
			return etatEditionRepository.findAll();
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public JSONObject getListGrouped() {
		try {
			List<EtatEdition> list = etatEditionRepository.findAll();
			List<String> listGroupe = etatEditionRepository.getListGroupes();

			List<EtatEdition> lf = new ArrayList<>();
			EtatEdition ee = new EtatEdition();
			for (String g : listGroupe) {
				ee.setGroupNameFr(g);
				ee.setChildren(list.stream().filter(e -> e.getGroupNameFr().equals(g)).collect(Collectors.toList()));
				ee.setGroupNameAr(ee.getChildren().get(0).getGroupNameAr());
			}

			JSONObject obj = new JSONObject();

			for (EtatEdition element : list) {
				if (obj.has(element.getGroupNameFr())) {
					obj.getJSONArray(element.getGroupNameFr()).put(element);
				} else {
					obj.put(element.getGroupNameFr(), new JSONArray(element));
				}
			}
			return obj;
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public EtatEdition findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new EtatEdition(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (EtatEdition) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public EtatEdition saveOrUpdate(EtatEdition entity) {
		try {
			return etatEditionRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public SendObject getListEtatEditionWs() {

		try {
			List<EtatEdition> list = etatEditionRepository.findAll();
			List<String> listGroupe = etatEditionRepository.getListGroupes();

			List<EtatEdition> lf = new ArrayList<>();
			for (String g : listGroupe) {
				EtatEdition ee = new EtatEdition();
				ee.setGroupNameFr(g);
				ee.setChildren(list.stream().filter(e -> e.getGroupNameFr().equals(g)).collect(Collectors.toList()));
				ee.setGroupNameAr(ee.getChildren().get(0).getGroupNameAr());
				lf.add(ee);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(lf));
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method getListEtatEditionWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			etatEditionRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findEtatEditionByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			EtatEdition entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method findEtatEditionByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateEtatEditionWs(EtatEdition entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method saveOrUpdateEtatEditionWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteEtatEditionByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method getListEtatEdition " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	public byte[] generatePdf(String reportName, String reposrtNameJasper, Map<String, Object> reportParameters)
			throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		reportParameters.put("SUB_DIR", "etat/");
		Connection connection = null;
		InputStream reportStream = new ClassPathResource(path + reportName).getInputStream();
		JasperReport jasper = JasperCompileManager.compileReport(reportStream);
		JRSaver.saveObject(jasper, reposrtNameJasper);
		connection = this.dataSource.getConnection();
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, reportParameters, connection);
		JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
		reportStream.close();
		connection.close();
		return baos.toByteArray();

	}

	public byte[] generateRtf(String reportName, String reposrtNameJasper, Map<String, Object> reportParameters)
			throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		reportParameters.put("SUB_DIR", "etat/");
		Connection connection = null;
		InputStream reportStream = new ClassPathResource(path + reportName).getInputStream();
		JasperReport jasper = JasperCompileManager.compileReport(reportStream);
		JRSaver.saveObject(jasper, reposrtNameJasper);
		connection = this.dataSource.getConnection();
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, reportParameters, connection);
		JRRtfExporter exporter = new JRRtfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		exporter.exportReport();
		reportStream.close();
		connection.close();
		return baos.toByteArray();

	}

	@Override
	public byte[] exportData(EtatEdition etat) {
		try {
			Map<String, Object> reportParameters = new HashMap<>();
			String reposrtName = "";
			String reposrtNameJasper = "";
			reportParameters.put("image_entete", "MEP_entete.jpg");
			for (ParamEtatEdition paramEtatEdition : etat.getParams()) {
				if (paramEtatEdition.getType().equalsIgnoreCase("Date")) {

					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					String dateAsString = paramEtatEdition.getValue();
					Date date = formatter.parse(dateAsString);
					reportParameters.put(paramEtatEdition.getCode(), date);

				} else if (paramEtatEdition.getType().equalsIgnoreCase("BigDecimal"))

					reportParameters.put(paramEtatEdition.getCode(),
							BigDecimal.valueOf(Long.valueOf(paramEtatEdition.getValue())));
				else
					reportParameters.put(paramEtatEdition.getCode(), Long.valueOf(paramEtatEdition.getValue()));
			}
			if (etat.getLang().equals("fr")) {
				reposrtName = etat.getTitreFr();
				reposrtNameJasper = etat.getLibelleFr();
			} else {
				reposrtName = etat.getTitreAr();
				reposrtNameJasper = etat.getLibelleAr();
			}
			if (etat.getType().equalsIgnoreCase("PDF"))
				return generatePdf(reposrtName, reposrtNameJasper, reportParameters);
			else
				return generateRtf(reposrtName, reposrtNameJasper, reportParameters);
		} catch (Exception e) {
			logger.error("Error EtatEditionService in method exportData :: " + e.toString());
			return null;
		}
	}


	@Override
	public SendObject getEtatFileToDownload(EtatEdition etat, String reportName) {
		File file = null;
		try {
			Map<String, Object> reportParameters = new HashMap<>();
			String reposrtNameJasper = "Etat fiche";
			reportParameters.put("image_entete", "MEP_entete.jpg");
			reportParameters.put("id",
					etat.getIdSearch() != null && !etat.getIdSearch().equals("")
							? BigDecimal.valueOf(Long.valueOf(etat.getIdSearch()))
							: null);

			byte[] bytes = null;
			if (etat.getType().equalsIgnoreCase("PDF"))
				bytes = generatePdf(reportName, reposrtNameJasper, reportParameters);
			else
				bytes = generateRtf(reportName, reposrtNameJasper, reportParameters);

			file = new File(reposrtNameJasper);
			FileUtils.writeByteArrayToFile(file, bytes);

			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, file);
		} catch (Exception e) {
			logger.error("Error getEtatFileToDownload in method getEtatFileToDownload :: " + e.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD, file);
		}
	}

}
