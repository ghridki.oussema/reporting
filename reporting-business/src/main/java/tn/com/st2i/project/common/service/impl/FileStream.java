package tn.com.st2i.project.common.service.impl;

import java.io.Serializable;


/**
 * Cette classe permet la génération d'un flux de sortie dans un contexte JSF (le téléchargement des
 * fichiers).
 *
 * @author St2i
 *
 */
public class FileStream implements Serializable
{

	private byte[] stream;

	private String fileName;

	private String contentType;

	/**
	 * Returns the encoded stream.
	 * 
	 * @return the encoded stream.
	 */
	public byte[] getStream()
	{
		return stream;
	}

	/**
	 * Sets the encoded stream.
	 * 
	 * @param stream the encoded stream.
	 */
	public void setStream(byte[] stream)
	{
		this.stream = stream;
	}

	/**
	 * Returns the report file name.
	 * 
	 * @return the report file name.
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**
	 * Sets the report file name.
	 * 
	 * @param fileName the report file name.
	 */
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	/**
	 * Returns the report content type.
	 * 
	 * @return the report content type
	 */
	public String getContentType()
	{
		return contentType;
	}

	/**
	 * Sets the report content type.
	 * 
	 * @param contentType the report content type
	 */
	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	@Override
	public String toString()
	{
		return "FileStreamUi{" + "fileName=" + fileName + ", contentType=" + contentType
			+ '}';
	}
}
