package tn.com.st2i.project.service;

import java.util.List;

import org.json.JSONObject;

import tn.com.st2i.project.model.EtatEdition;
import tn.com.st2i.project.tools.model.SendObject;

public interface IEtatEditionService {

	public List<EtatEdition> getList();

	public EtatEdition findById(Long id);

	public EtatEdition saveOrUpdate(EtatEdition entity);

	public Boolean deleteById(Long id);

	public SendObject findEtatEditionByIdWs(Long id);

	public SendObject getListEtatEditionWs();

	public SendObject saveOrUpdateEtatEditionWs(EtatEdition entity);

	public SendObject deleteEtatEditionByIdWs(Long id);

	public JSONObject getListGrouped();
	
	public byte[] exportData(EtatEdition etat) ;


	public SendObject getEtatFileToDownload(EtatEdition etat, String reportName);

	

}
