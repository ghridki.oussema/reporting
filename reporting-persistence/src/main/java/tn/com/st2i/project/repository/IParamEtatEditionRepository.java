package tn.com.st2i.project.repsitory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.st2i.project.model.ParamEtatEdition;

@Repository
public interface IParamEtatEditionRepository extends JpaRepository<ParamEtatEdition, Long> {

	@Query(value = "select * from reporting.param_etat_edition  a where exists (select 1 from reporting.etat_param b where a.id =b.id_param and  b.id_etat_edition=:idEtat)", nativeQuery = true)
	List<ParamEtatEdition> getListParamByIdEtat(@Param("idEtat") Long idEtat);
}
