package tn.com.st2i.project.view.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "v_adm_utilisateur", schema = "administration")
public class VAdmUser {

	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 255)
	private Long idAdmUser;

	@Column(name = "LOGIN", unique = true, nullable = false, length = 255)
	private String login;

	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "nom")
	private String name;

	@Column(name = "is_expire")
	private Boolean isExpire;

	@Column(name = "is_susp")
	private Boolean isSusp;
	
	@Column(name = "is_actif")
	private Boolean isActif;

	@Column(name = "is_actif_ar")
	private String isActifAr;

	@Column(name = "is_actif_fr")
	private String isActifFr;

	@Column(name = "id_adm_personnel")
	private Long idAdmPersonnel;

	@Column(name = "dt_ajout")
	private Timestamp dt_ajout;

	@Column(name = "dt_derniere_cnx")
	private Timestamp dt_derniere_cnx;

	@Column(name = "dt_expire")
	private Timestamp dt_expire;

	@Column(name = "dt_maj")
	private Timestamp dt_maj;

	@Column(name = "matricule")
	private String matricule;

	@Column(name = "mail")
	private String mail;

	@Column(name = "num_tel")
	private String num_tel;
	
}
