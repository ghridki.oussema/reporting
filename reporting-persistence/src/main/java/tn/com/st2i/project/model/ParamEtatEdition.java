package tn.com.st2i.project.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "param_etat_edition", schema = "reporting")

public class ParamEtatEdition implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "reporting.seq_param_etat_edition", name = "reporting.seq_param_etat_edition")
	@GeneratedValue(generator = "reporting.seq_param_etat_edition", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "code", nullable = false, length = 50)
	private String code;

	@Column(name = "libelle_ar", length = 150)
	private String libelleAr;

	@Column(name = "libelle_fr", length = 150)
	private String libelleFr;

	@Column(name = "libelle_en", length = 150)
	private String libelleEn;

	@Column(name = "ordre", nullable = false, precision = 22, scale = 0)
	private Long ordre;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name = "dt_maj", nullable = false, length = 13)
	private Date dtMaj;
	
	@Column(name = "type")
	private String type;


	@Transient
	private String value;

	public ParamEtatEdition clone() throws CloneNotSupportedException {
		return (ParamEtatEdition) super.clone();
	}
}
