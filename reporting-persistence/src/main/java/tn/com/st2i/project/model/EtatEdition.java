package tn.com.st2i.project.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "etat_edition", schema = "reporting")

public class EtatEdition implements java.io.Serializable, Cloneable {
    private transient static final long serialVersionUID = 1L;

    @SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "reporting.seq_etat_edition", name = "reporting.seq_etat_edition")
    @GeneratedValue(generator = "reporting.seq_etat_edition", strategy = GenerationType.SEQUENCE)
    @Id

    @Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
    private Long id;

    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @Column(name = "libelle_ar", length = 150)
    private String libelleAr;

    @Column(name = "libelle_fr", length = 150)
    private String libelleFr;

    @Column(name = "libelle_en", length = 150)
    private String libelleEn;

    @Column(name = "ordre", nullable = false, precision = 22, scale = 0)
    private Long ordre;

    @Column(name = "dt_maj", nullable = false, length = 13)
    private Date dtMaj;

    @Column(name = "titre_fr", length = 200)
    private String titreFr;

    @Column(name = "actif")
    private Boolean actif;

    @Column(name = "titre_ar", length = 150)
    private String titreAr;

    @Column(name = "group_name_fr", length = 150)
    private String groupNameFr;

    @Column(name = "group_name_ar", length = 150)
    private String groupNameAr;

	@Transient
	private List<ParamEtatEdition> params;

    @Transient
    private String lang;
    
    @Transient
    private String type;
    
    @Transient
    private String idSearch; 

    @Transient
    private List<EtatEdition> children;

    public EtatEdition clone() throws CloneNotSupportedException {
        return (EtatEdition) super.clone();
    }
}
