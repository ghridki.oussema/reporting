package tn.com.st2i.project.repsitory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tn.com.st2i.project.model.EtatEdition;

import java.util.List;

@Repository
public interface IEtatEditionRepository extends JpaRepository<EtatEdition, Long> {

    @Query(value=" select distinct a.groupNameFr  from  EtatEdition a")
    public List<String> getListGroupes();



}
